package com.colisweb.distances.caches

import cats.MonadError
import cats.effect.Sync
import com.colisweb.simplecache.memory.guava.GuavaCache
import com.colisweb.simplecache.wrapper.cats.CatsCache

import scala.concurrent.duration.FiniteDuration

object CatsGuavaCache {

  def async[F[_]: Sync, K, V](ttl: Option[FiniteDuration]): CatsCache[F, K, V] =
    CatsCache.async[F, K, V](new GuavaCache[K, V](ttl))

  def sync[F[_], K, V](ttl: Option[FiniteDuration])(implicit F: MonadError[F, Throwable]): CatsCache[F, K, V] =
    CatsCache.sync[F, K, V](new GuavaCache[K, V](ttl))
}
