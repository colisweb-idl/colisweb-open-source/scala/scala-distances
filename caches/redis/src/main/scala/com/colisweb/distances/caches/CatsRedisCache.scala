package com.colisweb.distances.caches

import cats.MonadError
import cats.effect.Sync
import com.colisweb.simplecache.redis.RedisConfiguration
import com.colisweb.simplecache.redis.RedisConfiguration.pool
import com.colisweb.simplecache.redis.circe.RedisCirceCache
import com.colisweb.simplecache.redis.codec._
import com.colisweb.simplecache.wrapper.cats.CatsCache

import scala.concurrent.duration.FiniteDuration

object CatsRedisCache {

  def async[F[_]: Sync, K, V](
      configuration: RedisConfiguration,
      ttl: Option[FiniteDuration],
      keyEncoder: Encoder[K] = AnyEncoder()
  )(implicit codec: io.circe.Codec[V]): CatsCache[F, K, V] =
    CatsCache.async(new RedisCirceCache[K, V](pool(configuration), ttl)(keyEncoder, codec))

  def sync[F[_], K, V](
      configuration: RedisConfiguration,
      ttl: Option[FiniteDuration],
      keyEncoder: Encoder[K] = AnyEncoder()
  )(implicit F: MonadError[F, Throwable], codec: io.circe.Codec[V]): CatsCache[F, K, V] =
    CatsCache.sync(new RedisCirceCache[K, V](pool(configuration), ttl)(keyEncoder, codec))
}
