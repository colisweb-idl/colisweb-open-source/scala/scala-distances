package com.colisweb.distances.cache

import cats.effect.IO
import com.colisweb.distances.Distances
import com.colisweb.distances.model.path.DirectedPath
import com.colisweb.distances.model.{PathResult, Point}
import com.colisweb.distances.util.FromMapDistances
import com.colisweb.simplecache.wrapper.cats.CatsCache
import org.mockito.IdiomaticMockito.StubbingOps
import org.mockito.scalatest.MockitoSugar
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec
import cats.effect.unsafe.implicits.global

class CacheSpec extends AnyWordSpec with Matchers with MockitoSugar {

  private val failingCache: CatsCache[IO, DirectedPath, PathResult] = mock[CatsCache[IO, DirectedPath, PathResult]]

  private val path: DirectedPath     = DirectedPath(Point(0, 0), Point(0, 0))
  private val pathResult: PathResult = PathResult(1, 1)

  private val distanceMap: Distances[IO, DirectedPath] =
    FromMapDistances[IO]
      .fromMap(Map(path -> pathResult))
      .caching(failingCache)

  "Failing cache" should {
    "not propagate failure on caching" in {
      val failure: IO[Nothing] = IO.raiseError(new RuntimeException("failure"))
      failingCache.get(any[DirectedPath]) returns failure
      failingCache.update(any[DirectedPath], any[PathResult]) returns failure
      distanceMap.api.distance(path).unsafeRunSync() shouldBe pathResult
    }
  }
}
